package ru.tsc.avramenko.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum EntityOperationType {

    POST_LOAD("Post Load"),
    POST_PERSIST("Post Persist"),
    POST_REMOVE("Post Remove"),
    POST_UPDATE("Post Update"),
    PRE_PERSIST("Pre Persist"),
    PRE_REMOVE("Pre Remove"),
    PRE_UPDATE("Pre Update");

    @NotNull
    public final String displayName;

    EntityOperationType(@NotNull String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}