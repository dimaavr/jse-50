package ru.tsc.avramenko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.avramenko.tm.api.service.IServiceLocator;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint() {
        super(null);
    }

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public TaskDTO bindTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @NotNull final String taskId
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getProjectTaskDtoService().bindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    public void createTask(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public void clearTask(
            @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @Override
    @Nullable
    public List<TaskDTO> findTaskAll(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getTaskDtoService().findAll(session.getUserId());
    }

    @Override
    @Nullable
    public TaskDTO findTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getTaskDtoService().findById(session.getUserId(), id);
    }

    @Override
    @Nullable
    public TaskDTO findTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getTaskDtoService().findByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    public TaskDTO findTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getTaskDtoService().findByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    public List<TaskDTO> findTaskByProjectId(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getProjectTaskDtoService().findTaskByProjectId(session.getUserId(), projectId);
    }

    @Override
    public TaskDTO finishTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getTaskDtoService().finishById(session.getUserId(), id);
    }

    @Override
    public TaskDTO finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getTaskDtoService().finishByIndex(session.getUserId(), index);
    }

    @Override
    public TaskDTO finishTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getTaskDtoService().finishByName(session.getUserId(), name);
    }

    @Override
    public void removeTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        serviceLocator.getTaskService().removeById(session.getUserId(), id);
    }

    @Override
    public void removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        serviceLocator.getTaskService().removeByIndex(session.getUserId(), index);
    }

    @Override
    public void removeTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        serviceLocator.getTaskService().removeByName(session.getUserId(), name);
    }

    @Override
    public TaskDTO changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getTaskDtoService().changeStatusById(session.getUserId(), id, status);
    }

    @Override
    public TaskDTO changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getTaskDtoService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    public TaskDTO changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getTaskDtoService().changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    public TaskDTO startTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getTaskDtoService().startById(session.getUserId(), id);
    }

    @Override
    public TaskDTO startTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getTaskDtoService().startByIndex(session.getUserId(), index);
    }

    @Override
    public TaskDTO startTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getTaskDtoService().startByName(session.getUserId(), name);
    }

    @Override
    public TaskDTO unbindTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @NotNull final String taskId
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getProjectTaskDtoService().unbindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    public TaskDTO updateTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "taskId", partName = "taskId") @NotNull final String taskId,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getTaskDtoService().updateById(session.getUserId(), taskId, name, description);
    }

    @Override
    public TaskDTO updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionDtoService().validate(session);
        return serviceLocator.getTaskDtoService().updateByIndex(session.getUserId(), index, name, description);
    }

}