package ru.tsc.avramenko.tm.api.repository.dto;

import ru.tsc.avramenko.tm.api.IRepositoryDto;
import ru.tsc.avramenko.tm.dto.UserDTO;

public interface IUserDtoRepository extends IRepositoryDto<UserDTO> {

    UserDTO findById(final String id);

    UserDTO findByLogin(final String login);

    UserDTO findByEmail(final String email);

    void removeById(final String id);

    void removeUserByLogin(final String login);

}