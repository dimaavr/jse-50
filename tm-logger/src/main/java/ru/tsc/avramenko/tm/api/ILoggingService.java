package ru.tsc.avramenko.tm.api;

import org.jetbrains.annotations.NotNull;

import javax.jms.JMSException;
import javax.jms.TextMessage;

public interface ILoggingService {

    void createBroadcastConsumer(@NotNull String className) throws JMSException;

    void writeLog(@NotNull TextMessage textMessage);

}